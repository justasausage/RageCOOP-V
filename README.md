
# 🌐 RAGECOOP
This is a reworked version of [RageCoop](https://github.com/RAGECOOP/RAGECOOP-V) with the original author's permission.


# **Help to survive the project**

#### The project is in active development as for now but may get discontinued if user's support is not present!

## if you are a developer...

Please **CONTRIBUTE** to the project and make RageCoop better together!

## otherwise...

**[Become a patreon](https://www.patreon.com/Sardelka)** to help keep the project alive and get exclusive support to individual issues.
<br><br><br>


# Features

1. Synchronized bullets
2. Synchronized vehicle/player/NPC
3. Synchronized Projectile
4. Basic ragdoll sync
5. Smoother vehicle/ped movement.
6. Ownership based sync logic, carjacking is now working properly.
7. Introduced SyncEvents.
8. Code refactoring and namespace cleanup
9. Synchronized vehicle doors, brake and throttle.
10. Other improvements

# Known issues

1. Weapon sounds are missing.
2. Cover sync is still buggy.
3. Weaponized vehicle doen't work currently
4. Framerate drop with high number of synchronized entities.
5. Scripting API is screwed.(will be rewritten in the future)

# Installation

## Client

Download latest release, remove old version of the mod. Extract the zip and put **RageCoop** in **Grand Theft Auto V/Scripts**.

## Server

Download latest release for your OS, then extract and run.

# Downloads

Download compiled binaries here: 
[Release](https://gitlab.com/justasausage/RageCOOP-V/-/tree/main/Release)

Please note that this is incompatible with all previous versions of ragecoop, remove old files before installing.
