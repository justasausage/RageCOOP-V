# Changelog for v0.2 alpha

- Synchronized projectile, all throwable weapons is now working properly, missiles will also be synchronized.
- Added customizable keybinding, you can now change menu key and passenger key in settings.
- Added option to disable FrontEndPauseAlternate (Esc), enabled by default.
- Fixed weapon that doesn't have any components not displaying properly.
- Small fix for passenger sync.
- Added linux-x64, linux-arm and osx-x64 builds
- Fixed wrong file and directory path, you may need to remove old versions before installing.
- You can now change LogLevel in RageCoop.Client.Settings.xml. (default 2)
- Other player's latency should now display properly.

# Known issues
- Sticky bomb will randomly explode if attached to other player's vehicle

